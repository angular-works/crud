import { Component, OnInit } from '@angular/core';
import { Home } from '../home'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  Alldata: any;
  Resultdata: any;
  info = new Home('', '', '', '', '');
  add_block: boolean;
  studentData: { email: string; }[];
  emails: string;
  
  constructor() { }

  ngOnInit() {
    // this.add_block = false;
    this.Resultdata =[
      {id: 1, firstname: "Nagasreenu ",lastname: "ch",email: "nagasreenu@gmail.com", address: "hyderbad"},
      {id: 2, firstname: "Praveen ",lastname: "ch",email: "nagasreenu@gmail.com", address: "Pithapuram"},
      {id: 3, firstname: "Phani ",lastname: "ch",email: "nagasreenu@gmail.com", address: "Vijayawada"}
    ];
  
    this.Alldata = this.Resultdata;
    this.studentData = [
      {
        email: ''
      }
    ];
  }
  searchItem(searchvalue){
    debugger;
    this.Alldata = this.Resultdata;
    let entervalue = searchvalue.target.value;
    this.Alldata = this.Alldata.filter((item)=>item.firstname.toLowerCase().startsWith(entervalue));
    console.log(entervalue);

  }
  addCall(info){
    debugger;
    this.emails = '';
    for (let i = 0; i < this.studentData.length; i++) {
      if (i + 1 === this.studentData.length) {
        this.emails = this.emails + this.studentData[i].email;
      } else {
        this.emails = this.emails + this.studentData[i].email + ',';
      }
    } 

    const obj = {
      firstname: info.firstname,
      lastname: info.lastname,
      email: this.emails,
      address: info.address
    }

    this.Resultdata.push(obj);
    this.info = new Home('', '', '', '', '');
    this.studentData = [
      {
        email: ''
      }
    ];
    console.log(obj)
  }


  add(){
    this.add_block = true;
  }

  deletecall(i){
    this.Alldata.splice(i, 1);
  }

  Editcall(index){
    debugger;
    let value  = this.Resultdata[index];
    debugger;
    this.info.id = value.id
    this.info.firstname =value.firstname
    this.info.lastname =value.lastname
    this.info.email =value.email
    this.info.address =value.address
  }

  editCall(obj){
    debugger;
    for(let i=0;i<this.Resultdata.length;i++){
      if(obj.id ===this.Resultdata[i].id ){
       this.Resultdata[i].id = obj.id;
       this.Resultdata[i].firstname = obj.firstname;
       this.Resultdata[i].lastname = obj.lastname;
       this.Resultdata[i].email = obj.email;
       this.Resultdata[i].address = obj.address;
       this.info = new Home('', '', '', '', '');
      }
 }
  }
  add_email() {
    debugger;
    const obj = {
      email: ''
    };
    this.studentData.push(obj);
    console.log(this.studentData);
  }

}
