export class Home {
    constructor(
        public firstname: string,
        public lastname: string,
        public email: string,
        public address: string,
        public id: any
    ) { }
}
